import graphene
# from hero.schema import HeroQuery
from products.schema import CategoryQuery, CategoryMutation, ProductQuery, ProductMutation

class Query(CategoryQuery, ProductQuery, graphene.ObjectType):
    pass


class Mutation(CategoryMutation, ProductMutation, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
