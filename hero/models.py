from django.db import models


class Hero(models.Model):
    GENDER_CHOICES = (
        ('M', 'MALE'),
        ('F', 'FEMALE'),
    )

    name = models.CharField(max_length=64, blank=True)
    gender = models.CharField(max_length=16, choices=GENDER_CHOICES, null=True, default='F')
    movie = models.CharField(max_length=128, blank=True)

    class Meta:
        verbose_name = 'Hero'
        verbose_name_plural = 'Heroes'
        ordering = ['name']

    def __str__(self):
        if self.name:
            return self.name
        else:
            return f'Hero-{self.pk}'
