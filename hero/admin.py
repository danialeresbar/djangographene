from django.contrib import admin
from hero.models import Hero


@admin.register(Hero)
class HeroAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name', 'gender', 'movie')
        }),
    )
    list_display = ('id', 'name', 'gender', 'movie')
    list_editable = ('name', 'gender')
    list_filter = ('gender',)
