from django.contrib import admin
from products.models import Category, Product


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_editable = ('name',)


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name',)
        }),
        ('Metadata', {
            'fields': (('price', 'created', 'in_stock'), 'categories')
        })
    )
    list_display = ('id', 'name', 'price', 'in_stock', 'created')
    list_editable = ('name', 'price', 'in_stock')
    list_filter = ('in_stock', 'created')
    filter_horizontal = ('categories',)
