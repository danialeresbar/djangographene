import graphene
from graphene import Argument
from graphene_django.types import DjangoObjectType
from products.models import Category, Product


class CategoryType(DjangoObjectType):
    class Meta:
        model = Category


class ProductType(DjangoObjectType):
    class Meta:
        model = Product


# --------QUERIES--------

# ----Categories----
class CategoryQuery(graphene.ObjectType):
    all_categories = graphene.List(CategoryType)
    get_category = graphene.Field(CategoryType, id=graphene.ID())

    def resolve_all_categories(self, info, **kwargs):
        return Category.objects.all()

    def resolve_get_category(self, info, **kwargs):
        return Category.objects.get(pk=kwargs.get('id'))


# ----Products----
class ProductQuery(graphene.ObjectType):
    all_products = graphene.List(ProductType)
    get_product = graphene.Field(ProductType, id=graphene.ID())

    def resolve_all_products(self, info, **kwargs):
        return Product.objects.all()

    def resolve_get_product(self, info, **kwargs):
        return Product.objects.get(pk=kwargs.get('id'))


# --------MUTATIONS--------

# ----Categories----
class CreateCategory(graphene.Mutation):
    # Let's define the arguments we can pass the create method
    class Arguments:
        name = graphene.String()

    category = graphene.Field(CategoryType)

    def mutate(self, info, **kwargs):
        name = kwargs.get('name', None)
        category = Category.objects.create(name=name)
        return CreateCategory(category=category)


class UpdateCategory(graphene.Mutation):
    # Let's define the arguments we can pass the create method
    class Arguments:
        id = graphene.ID()
        name = graphene.String()

    category = graphene.Field(CategoryType)

    def mutate(self, info, **kwargs):
        id = kwargs.get('id', None)
        name = kwargs.get('name', None)

        try:
            category = Category.objects.get(pk=id)
            category.name = name if name is not None else category.name
            category.save()
            return UpdateCategory(category=category)
        except Exception as e:
            print(f'Error updating category: \n{e}')


class DeleteCategory(graphene.Mutation):
    # Let's define the arguments we can pass the create method
    class Arguments:
        id = graphene.ID()

    category = graphene.Field(CategoryType)

    def mutate(self, info, **kwargs):
        id = kwargs.get('id', None)
        try:
            category = Category.objects.get(pk=id)
            category.delete()
            return DeleteCategory(category=category)
        except Exception as e:
            print(f'Error deleting product: \n{e}')


# ----Products----
class CreateProduct(graphene.Mutation):
    # Let's define the arguments we can pass the create method
    class Arguments:
        name = graphene.String()
        price = graphene.Float()
        categories = graphene.List(graphene.ID)
        in_stock = graphene.Boolean()
        created = graphene.types.datetime.DateTime()

    product = graphene.Field(ProductType)

    def mutate(self, info, **kwargs):
        name = kwargs.get('name', None)
        price = float(kwargs.get('price', 0))
        in_stock = kwargs.get('in_stock', None)
        created = kwargs.get('created', None)

        # Let's create a new product
        product = Product.objects.create(
            name=name,
            price=price,
            in_stock=in_stock,
            created=created
        )

        # This is how we deal with ManyToMany
        categories = kwargs.get('categories', None)
        if categories is not None:
            categories_set = []
            for category_id in categories:
                categories_set.append(Category.objects.get(pk=category_id))
            product.categories.set(categories_set)

        product.save()
        return CreateProduct(product=product)


class UpdateProduct(graphene.Mutation):
    # The input argument for this mutation
    class Arguments:
        id = graphene.ID()
        name = graphene.String()
        price = graphene.Float()
        categories = graphene.List(graphene.ID)
        in_stock = graphene.Boolean()
        created = graphene.types.datetime.DateTime()

    product = graphene.Field(ProductType)

    def mutate(self, info, **kwargs):
        id = kwargs.get('id', None)
        name = kwargs.get('name', None)
        price = kwargs.get('price', None)
        categories = kwargs.get('categories', [])
        in_stock = kwargs.get('in_stock', None)
        created = kwargs.get('created', None)

        try:
            product = Product.objects.get(pk=id)
            product.name = name if name is not None else product.name
            product.price = price if price is not None else product.price
            product.in_stock = in_stock if in_stock is not None else product.in_stock
            product.created = created if created is not None else product.created

            # Loop through and update categories for our product
            if categories:
                categories_set = []
                for category_id in categories:
                    categories_set.append(Category.objects.get(pk=category_id))
                product.categories.set(categories_set)
                product.save()
                return UpdateProduct(product=product)
        except Exception as e:
            print(f'Error updating product: \n{e}')


class DeleteProduct(graphene.Mutation):
    # The input arguments for this mutation
    class Arguments:
        id = graphene.ID()

    product = graphene.Field(ProductType)

    def mutate(self, info, **kwargs):
        id = kwargs.get('id', None)
        try:
            product = Product.objects.get(pk=id)
            product.delete()
            return DeleteProduct(product=product)
        except Exception as e:
            print(f'Error deleting product: \n{e}')


class CategoryMutation(graphene.ObjectType):
    create_category = CreateCategory.Field()
    update_category = UpdateCategory.Field()
    delete_category = DeleteCategory.Field()


class ProductMutation(graphene.ObjectType):
    create_product = CreateProduct.Field()
    update_product = UpdateProduct.Field()
    delete_product = DeleteProduct.Field()
